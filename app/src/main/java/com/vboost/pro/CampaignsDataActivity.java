package com.vboost.pro;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnDismissListener;
import com.bigkoo.alertview.OnItemClickListener;
import com.squareup.picasso.Picasso;
import com.vboost.pro.Adapter.CampaignsDataAdapter;
import com.vboost.pro.DBClasses.Company;
import com.vboost.pro.DBClasses.DataLabels;
import com.vboost.pro.model.Campaigns;
import com.vboost.pro.model.Labels;
import com.vboost.pro.model.ManageContacts;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Nabeel Hafeez on 8/10/2016.
 */
public class CampaignsDataActivity extends AppCompatActivity implements CampaignsDataAdapter.CallbackInterface, OnItemClickListener, OnDismissListener {

    ArrayList<ManageContacts> contactsList;
    ArrayList<Labels> labalesArrayList;
    ArrayList<Campaigns> campaignsArray;
    ArrayList<String> labalesDBArrayList;
    Uri[] imagesArray;
    Campaigns campaign;
    String[] contactsName;
    ListView cameraImageList;
    String yourName;
    int minCount;
    int maxCount;
    int defaultCount;
    int campaingID;
    int contactID;

    String emailPattern;
    int keyDel;

    Button phoneTextButton;
    Button emailTextButton;
    Button nextButton;
    Button nameText;
    EditText phoneText;
    EditText emailText;
    AlertView selectName;
    AlertView campaignDataMenu;
    AlertView DataMenu;

    ScrollView scrollView;

    String editTextFlag;
    String deliverText;

    Uri imageUri = null;

    private static final int PERMISSION_REQUEST_CODE = 1;
    public static final int MY_REQUEST = 1001;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_campaigns_data);

        List<Company> company =  Company.getAll();

        ImageView companyLogo = (ImageView) findViewById(R.id.campaigns_data_logo);

        Picasso.with(this)
                .load(company.get(0).getLogo())
                .error(R.drawable.logo2)
                .into(companyLogo);

        contactsName = getIntent().getStringArrayExtra("contactsName");
        contactsList = (ArrayList<ManageContacts>) getIntent().getSerializableExtra("contacts");
        campaign = (Campaigns) getIntent().getSerializableExtra("campaignrow");
        campaignsArray = (ArrayList<Campaigns>) getIntent().getSerializableExtra("campaignArray");

        emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        scrollView = (ScrollView) findViewById(R.id.campaigns_data_scroll_view);
        phoneTextButton = (Button) findViewById(R.id.campaigns_data_phone_button);
        emailTextButton = (Button) findViewById(R.id.campaigns_data_mail_button);
        nextButton = (Button) findViewById(R.id.campaigns_data_next_button);
        nextButton.setAllCaps(false);
        cameraImageList = (ListView) findViewById(R.id.list_view_campaigns_images);
        nameText = (Button) findViewById(R.id.contact_name);
        nameText.setAllCaps(false);
        phoneText = (EditText) findViewById(R.id.phone_edit_text);
        emailText = (EditText) findViewById(R.id.email_edit_text);

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermission()) {

                Log.e("value", "Permission already Granted, Now you can get image.");
            } else {
                requestPermission();
            }
        } else {

            Log.e("value", "Not required for requesting runtime permission");
        }

        selectName = new AlertView(null, null, null, null,
                contactsName,
                this, AlertView.Style.ActionSheet, this);

        campaignDataMenu = new AlertView(null, null, null, null,
                new String[]{"Change Customer Type",
                        "Manage Contacts",
                        "Switch Companies",
                        "Get Help",
                },
                this, AlertView.Style.Alert, this).setCancelable(true).setOnDismissListener(this);

        DataMenu = new AlertView(null, null, null, null,
                new String[]{
                        "Manage Contacts",
                        "Switch Companies",
                        "Get Help",
                },
                this, AlertView.Style.Alert, this).setCancelable(true).setOnDismissListener(this);

        scrollView.bringToFront();
        minCount = campaign.getImage().getMin_count();
        maxCount = campaign.getImage().getMin_count();
        defaultCount = campaign.getImage().getDefault_count();
        labalesArrayList = campaign.getImage().getLabels();
        campaingID = campaign.getId();


        imagesArray = new Uri[defaultCount];

        phoneTextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#569fd7"), 0x00000000));
        emailTextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#569fd7"), 0x00000000));
        nextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#FBA919"), 0x00000000));

        List<DataLabels> dataLabels;
        dataLabels = DataLabels.getAll();
        if (dataLabels.size() != 0) {

            for (int i = 0; i < dataLabels.size(); i++) {

                String imageString = dataLabels.get(i).getImage();

                imagesArray[i] = Uri.parse(imageString);
            }

            cameraImageList.setAdapter(new CampaignsDataAdapter(CampaignsDataActivity.this, imagesArray, minCount, maxCount, defaultCount, labalesArrayList, true, false));


        } else {
            if (isOnline(this)) {
                cameraImageList.setAdapter(new CampaignsDataAdapter(CampaignsDataActivity.this, imagesArray, minCount, maxCount, defaultCount, labalesArrayList, false, false));
            } else {

            }
        }
    }

    public void enterPhoneNumber(View view) {

        phoneTextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"), 0x00000000));
        emailTextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#569fd7"), 0x00000000));
        phoneText.setHint(R.string.delivery_phone_text_method);
        phoneText.setVisibility(View.VISIBLE);
        emailText.setVisibility(View.INVISIBLE);
        phoneText.setSingleLine();
        phoneText.setInputType(InputType.TYPE_CLASS_PHONE);

        phoneText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(phoneText, InputMethodManager.SHOW_IMPLICIT);

        editTextFlag = "phone";

        phoneText.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                boolean flag = true;
                String eachBlock[] = phoneText.getText().toString().split("-");
                for (int i = 0; i < eachBlock.length; i++) {
                    if (eachBlock[i].length() > 3) {
                        //Log.v("11111111111111111111","cc" + flag + eachBlock[i].length());
                    }
                }
                if (flag) {
                    phoneText.setOnKeyListener(new View.OnKeyListener() {

                        public boolean onKey(View v, int keyCode, KeyEvent event) {

                            if (keyCode == KeyEvent.KEYCODE_DEL)
                                keyDel = 1;
                            return false;
                        }
                    });

                    if (keyDel == 0) {

                        if (((phoneText.getText().length() + 1) % 4) == 0) {
                            if (phoneText.getText().toString().split("-").length <= 2) {
                                phoneText.setText(phoneText.getText() + "-");
                                phoneText.setSelection(phoneText.getText().length());
                            }
                        }
                        deliverText = phoneText.getText().toString();
                    } else {
                        deliverText = phoneText.getText().toString();
                        keyDel = 0;
                    }

                } else {
                    phoneText.setText(deliverText);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            public void afterTextChanged(Editable s) {


            }


        });

    }

    public void enterEmail(View view) {

        phoneTextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#569fd7"), 0x00000000));
        emailTextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"), 0x00000000));
        emailText.setHint(R.string.delivery_mail_text_method);
        emailText.setVisibility(View.VISIBLE);
        phoneText.setVisibility(View.INVISIBLE);
        emailText.setSingleLine();
        emailText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        editTextFlag = "email";

        emailText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(emailText, InputMethodManager.SHOW_IMPLICIT);

    }

    public void campaignsPermissionButton(View view) {
        String campaignName = nameText.getText().toString();
        //campaignName =  campaignName.trim();
        boolean imagesArrayUncomplete = false;
        for(Uri image : imagesArray)
        {
            if(image == null){
                imagesArrayUncomplete = true;
                break;
            }
        }
        if (imagesArrayUncomplete) {

            new AlertView(null, "\nPlease take at least " + defaultCount + " photos.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

        } else {
            if (campaignName.equals("Select Your Name")) {

                new AlertView(null, "\nPlease select your name from the \ndropdown menu.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

            } else {

                if (editTextFlag != null) {
                    if (editTextFlag.equals("email")) {
                        deliverText = emailText.getText().toString();
                        if (deliverText.matches(emailPattern)) {

                            Intent sharePermissionIntent = new Intent(CampaignsDataActivity.this, SharePermissionActivity.class);
                            sharePermissionIntent.putExtra("name", nameText.getText().toString());
                            sharePermissionIntent.putExtra("deliverMethod", deliverText);
                            sharePermissionIntent.putExtra("campaignID", campaingID);
                            sharePermissionIntent.putExtra("contactID", contactID);
                            sharePermissionIntent.putExtra("deliveryFlag", editTextFlag);
                            sharePermissionIntent.putExtra("contactsName",contactsName);
                            sharePermissionIntent.putExtra("campaignrow",campaign);
                            sharePermissionIntent.putExtra("contacts",contactsList);
                            sharePermissionIntent.putExtra("campaignArray", campaignsArray);
                            if(campaignsArray.size() ==1){
                                sharePermissionIntent.putExtra("singleCampaign", true);
                            }else {
                                sharePermissionIntent.putExtra("singleCampaign", false);
                            }
                            startActivity(sharePermissionIntent);

                            finish();


                        } else {

                            new AlertView(null, "\nPlease enter valid email address!", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

                        }

                    } else {
                        deliverText = phoneText.getText().toString();
                        if (deliverText.length() < 12) {
                            new AlertView(null, "\nPlease Enter 10 digit phone number!", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

                        } else {

                            Intent sharePermissionIntent = new Intent(CampaignsDataActivity.this, SharePermissionActivity.class);
                            sharePermissionIntent.putExtra("name", nameText.getText().toString());
                            sharePermissionIntent.putExtra("deliverMethod", deliverText);
                            sharePermissionIntent.putExtra("campaignID", campaingID);
                            sharePermissionIntent.putExtra("contactID", contactID);
                            sharePermissionIntent.putExtra("deliveryFlag", editTextFlag);
                            sharePermissionIntent.putExtra("contactsName",contactsName);
                            sharePermissionIntent.putExtra("campaignrow",campaign);
                            sharePermissionIntent.putExtra("contacts",contactsList);
                            sharePermissionIntent.putExtra("campaignArray", campaignsArray);
                            if(campaignsArray.size() ==1){
                                sharePermissionIntent.putExtra("singleCampaign", true);
                            }else {
                                sharePermissionIntent.putExtra("singleCampaign", false);
                            }
                            startActivity(sharePermissionIntent);

                            finish();

                        }

                    }

                } else {
                    new AlertView(null, "\nPlease select the delivery method.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

                }
            }
        }
    }

    public void selectYourName(View view) {

        selectName.show();
    }

    public void closeActivity(View view) {

        finish();

    }

    public void alertMenuCampaignData(View view) {
        if (campaignsArray.size() == 1) {
            DataMenu.show();
        } else {
            campaignDataMenu.show();
        }
    }

    @Override
    public void onItemClick(Object o, int position) {

        if (o == selectName && position != AlertView.CANCELPOSITION) {
            for (int i = 0; i < contactsName.length; i++) {
                if (position == i) {

                    yourName = contactsName[i];
                    contactID = contactsList.get(i).getId();
                }
            }
            nameText.setText(yourName);
        }

        if (o == campaignDataMenu && position != AlertView.CANCELPOSITION) {
            if (position == 0) {
                Intent contactIntent = new Intent(CampaignsDataActivity.this, CampaignsActivity.class);
                contactIntent.putExtra("contactsName", contactsName);
                contactIntent.putExtra("contacts", contactsList);
                contactIntent.putExtra("campaignArray", campaignsArray);
                startActivity(contactIntent);
                finish();

            } else if (position == 1) {

                Intent contactIntent = new Intent(CampaignsDataActivity.this, ManageContatcsAvtivity.class);
                startActivity(contactIntent);
                finish();

            } else if (position == 2) {

                Intent logoutIntent = new Intent(CampaignsDataActivity.this, LogOutActivity.class);
                startActivity(logoutIntent);
                finish();

            } else if (position == 3) {

                Intent helpIntent = new Intent(CampaignsDataActivity.this, GetHelpActivity.class);
                startActivity(helpIntent);

            }
        }

        if (o == DataMenu && position != AlertView.CANCELPOSITION) {
           if(position == 0) {

                Intent contactIntent = new Intent(CampaignsDataActivity.this, ManageContatcsAvtivity.class);
                startActivity(contactIntent);
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                   finishAffinity();
               }
               finish();

            } else if (position == 1) {

                Intent logoutIntent = new Intent(CampaignsDataActivity.this, LogOutActivity.class);
                startActivity(logoutIntent);
               finish();

            } else if (position == 2) {

                Intent helpIntent = new Intent(CampaignsDataActivity.this, GetHelpActivity.class);
                startActivity(helpIntent);
            }
        }


    }

    @Override
    public void onDismiss(Object o) {


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            if(mAlertView!=null && mAlertView.isShowing()){
//                mAlertView.dismiss();
//                return false;
//            }
        }

        return super.onKeyDown(keyCode, event);

    }

   /* @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK) {
                String path = getIntent().getStringExtra("imagePath");
                images.add(path);
                Log.e("Activity Result Call", path);
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }

    }*/

    /**
     * Interface Method which communicates to the Acitivty here from the {@link }
     *
     * @param position - the position
     * @param text     - the text to pass back
     */
    @Override
    public void onHandleSelection(int position, String text) {
        boolean editPhoto;

        if (imagesArray[position] != null) {

            editPhoto = true;
            String imagePath = imagesArray[position].toString();
            Intent intent = new Intent(CampaignsDataActivity.this, PhotoResultActivity.class);
            intent.putExtra("imagepath", imagePath);
            intent.putExtra("phototitle", text);
            intent.putExtra("Position", position);
            intent.putExtra("editphoto", editPhoto);
            startActivityForResult(intent, MY_REQUEST);

        } else {

            editPhoto = false;

            Long cameraClickTime = System.currentTimeMillis();
            String fileNameExtension = cameraClickTime + ".jpg";

            //  destinationFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)+File.separator+"Camera"+File.separator, fileNameExtension);

            String fileName = fileNameExtension;

            // Create parameters for Intent with filename

            ContentValues values = new ContentValues();

            values.put(MediaStore.Images.Media.TITLE, fileName);

            values.put(MediaStore.Images.Media.DESCRIPTION, "Image capture for Customer and Vehicle");

            /****** imageUri is the current activity attribute, define and save it for later usage  *****/
            imageUri = getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            // cameraIntent.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            cameraIntent.putExtra("phototitle", text);
            cameraIntent.putExtra("Position", position);
            cameraIntent.putExtra("editphoto", editPhoto);
            startActivityForResult(cameraIntent, MY_REQUEST);

            /*Intent intent = new Intent(CampaignsDataActivity.this, CameraActivity.class);
            intent.putExtra("phototitle", text);
            intent.putExtra("Position", position);
            intent.putExtra("editphoto", editPhoto);
            startActivityForResult(intent, MY_REQUEST);*/

        }
    }


// use of nonNullElemExist;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (resultCode) {

            case RESULT_OK:

                // ... Check for some data from the intent
                if (requestCode == MY_REQUEST) {
                    // .. lets toast again
                    int position = -1;
                    String imagePath = "";
                    if (data != null) {
                        position = data.getIntExtra("Position", 0);
                       // imagePath = data.getStringExtra("imagePath");
                        Log.e("Image URI", ""+imageUri);
                        if(imagePath.equals("")|| imagePath == null){
                            imagesArray[position] = null;
                        }else {
                            imagesArray[position] =imageUri;
                        }
                    }

                    if (position != -1) {
                        new LabelDataSave(imagePath, position, labalesArrayList.get(position).getName(), labalesArrayList.get(position).getTitle(), String.valueOf(campaingID), null).execute();
                               /* cameraImageList.setAdapter(new CampaignsDataAdapter(CampaignsDataActivity.this , imagesArray , minCount , maxCount , defaultCount , labalesArrayList, false , false));
                                String imageAsString = imageStringFormat(imagePath);
                                DataLabels dataLabels = new DataLabels(imageAsString,position, labalesArrayList.get(position).getName(), labalesArrayList.get(position).getTitle(), null, null);
                                dataLabels.saveToDb();*/

                        final int newposition = position + 1;

                        CountDownTimer timer = new CountDownTimer(1000, 1000) {

                            public void onTick(long millisUntilFinished) {

                            }

                            public void onFinish() {
                                if (newposition < defaultCount && imagesArray[newposition] == null) {

                                    Intent intent = new Intent(CampaignsDataActivity.this, CameraActivity.class);
                                    intent.putExtra("phototitle", labalesArrayList.get(newposition).getTitle());
                                    intent.putExtra("Position", newposition);
                                    startActivityForResult(intent, MY_REQUEST);
                                }
                            }
                        };
                        timer.start();

                        // Toast.makeText(this, "Handled the result successfully at position " + position, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "Failed to get data from intent", Toast.LENGTH_SHORT).show();
                    }
                }

                break;

            case RESULT_CANCELED:

                // ... Handle this situation
                break;
        }
    }



       /*Runtime Permission Code Start Here*/

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(CampaignsDataActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(CampaignsDataActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(CampaignsDataActivity.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(CampaignsDataActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("value", "Permission Granted, Now you can save image .");
                } else {
                    Log.e("value", "Permission Denied, You cannot save image.");
                }
                break;
        }
    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }

    public class LabelDataSave extends AsyncTask<String, String, String> {

        DataLabels dataLabels;
        String image;
        int imageSequence;
        String name;
        String title;
        String campaignOwner;
        String labelPreference;


        public LabelDataSave(String _image,
                             int _imageSequence,
                             String _name,
                             String _title,
                             String _campaignOwner,
                             String _labelPreference) {

            this.image = _image;
            this.imageSequence = _imageSequence;
            this.name = _name;
            this.title = _title;
            this.campaignOwner = _campaignOwner;
            this.labelPreference = _labelPreference;

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {

            // String imageAsString = imageStringFormat(image);
            dataLabels = new DataLabels(image, imageSequence, name, title, campaignOwner, labelPreference);
            dataLabels.saveToDb();

            return null;
        }

        @Override
        protected void onPostExecute(String unused) {

            cameraImageList.setAdapter(new CampaignsDataAdapter(CampaignsDataActivity.this, imagesArray, minCount, maxCount, defaultCount, labalesArrayList, false, false));
        }
    }

}
