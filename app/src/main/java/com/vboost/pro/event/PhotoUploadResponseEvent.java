package com.vboost.pro.event;

/**
 * @author Samvel Abrahamyan
 */

public class PhotoUploadResponseEvent {

    private String message;
    private boolean success;

    public PhotoUploadResponseEvent(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
