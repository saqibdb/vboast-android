package com.vboost.pro.event;

import com.vboost.pro.model.PhotoAndName;

import java.util.ArrayList;

/**
 * @author Samvel Abrahamyan
 */

public class PhotoUploadRequestEvent {

    //private List<byte[]> photos;
    private ArrayList<PhotoAndName> photoAndName;
    private int campaignID;
    private int contactID;
    private String recipientName;
    private String recipientEmail;
    private Integer permission;
    private String recipientPhone;
    private String recipientSignature;




    public PhotoUploadRequestEvent(/*List<byte[]> _photos*/ArrayList<PhotoAndName> _photoAndName,
                                   int _campaignID,
                                   int _contactID,
                                   String _recipientName,
                                   String _recipientEmail,
                                   Integer _permission,
                                   String _recipientPhone,
                                   String _recipientSignature
                                   ) {
        /*this.photos = _photos;*/
        this.photoAndName = _photoAndName;
        this.campaignID = _campaignID;
        this.contactID = _contactID;
        this.recipientName = _recipientName;
        this.recipientEmail = _recipientEmail;
        this.permission = _permission;
        this.recipientPhone = _recipientPhone;
        this.recipientSignature = _recipientSignature;

    }

    /*public List<byte[]> getPhotos() {
        return photos;
    }*/

    public int getCampaignID() {
        return campaignID;
    }

    public int getContactID() {
        return contactID;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public String getRecipientPhone() {
        return recipientPhone;
    }

    public Integer getPermission() {
        return permission;
    }

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public String getRecipientSignature() {
        return recipientSignature;
    }

    public ArrayList<PhotoAndName> getPhotoAndName() {
        return photoAndName;
    }
}
