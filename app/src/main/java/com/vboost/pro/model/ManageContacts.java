package com.vboost.pro.model;

/**
 * Created by Nabeel on 08/09/2016.
 */


import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;


public class ManageContacts implements Serializable {

    private  int id;
    private  int company;
    private String name;
    private String is_active;
    private String title;
    private String type;
    private String email;
    private String phone;
    private String photo;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompany() {
        return company;
    }

    public void setCompany(int company) {
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public ManageContacts(int _company,
                   int _contact_id,
                   String _email,
                   String _is_active,
                   String _name,
                   String _phone,
                   String _photo,
                   String _title,
                   String _type) {

        this.setCompany(_company);
        this.setId(_contact_id);
        this.setEmail(_email);
        this.setIs_active(_is_active);
        this.setName(_name);
        this.setPhone(_phone);
        this.setPhoto(_photo);
        this.setTitle(_title);
        this.setType(_type);

    }


    public ManageContacts(JSONObject dict) throws JSONException {
        this.setWithResponseDict(dict);
    }

    public ManageContacts setWithResponseDict(JSONObject dict) throws JSONException {

        if (dict.has("id")) {
            this.id = dict.getInt("id");
            if (dict.isNull("id")) {
                this.id = 0;
            }
        }
        if (dict.has("company")) {
            this.company = dict.getInt("company");
            if (dict.isNull("company")) {
                this.company = 0;
            }
        }

        if (dict.has("name")) {
            this.name = dict.getString("name");
            if (dict.isNull("name")) {
                this.name = "";
            }
        }
        if (dict.has("is_active")) {
            this.is_active = dict.getString("is_active");
            if (dict.isNull("is_active")) {
                this.is_active = "";
            }
        }

        if (dict.has("title")) {
            this.title = dict.getString("title");
            if (dict.isNull("title")) {
                this.title = "";
            }
        }

        if (dict.has("type")) {
            this.type = dict.getString("type");
            if (dict.isNull("type")) {
                this.type = "";
            }
        }

        if (dict.has("email")) {
            this.email = dict.getString("email");
            if (dict.isNull("email")) {
                this.email = "";
            }
        }

        if (dict.has("phone")) {
            this.phone = dict.getString("phone");
            if (dict.isNull("phone")) {
                this.phone = "";
            }
        }

        if (dict.has("photo")) {
            this.photo = dict.getString("photo");
            if (dict.isNull("photo")) {
                this.photo = "";
            }
        }


        return this;
    }
}


