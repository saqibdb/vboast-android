package com.vboost.pro.model;

import java.util.List;

/**
 * Created by iBuildX on 11/22/2016.
 */

public class PhotoAndName {
    private byte[] photos;
    private String name;

    public PhotoAndName(/*byte[] _photos, String _name*/) {
        /*this.setPhotos(_photos);
        this.setName(_name);*/

    }

    public byte[] getPhotos() {
        return photos;
    }

    public void setPhotos(byte[] photos) {
        this.photos = photos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
