package com.vboost.pro.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Nabeel Hafeez on 8/9/2016.
 */
public class Campaigns implements Serializable {

    private int id;
    private int company;
    private String name;
    private String key;
    private String logo;
    private String details;
    private String color;
    private Images image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCompany() {
        return company;
    }

    public void setCompany(int company) {
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Images getImage() {
        return image;
    }

    public void setImage(Images image) {
        this.image = image;
    }

    public Campaigns(String _campaign_Color,
                    String _campaign_Details,
                    int _campaignID,
                    int _company,
                    int _default_count,
                    String _key,
                    String _logo,
                    int _max_count,
                    int _min_count,
                    String _name) {
        Images images = new Images(_min_count, _max_count, _default_count, null);
        this.setColor(_campaign_Color);
        this.setDetails(_campaign_Details);
        this.setId(_campaignID);
        this.setCompany(_company);
        this.setImage(images);
        this.setKey(_key);
        this.setLogo(_logo);
        this.setName(_name);
    }

    public Campaigns(JSONObject dict) throws JSONException {
        this.setWithResponseDict(dict);
    }

    public Campaigns setWithResponseDict(JSONObject dict) throws JSONException {

        if (dict.has("id")) {
            this.id = dict.getInt("id");
            if (dict.isNull("id")) {
                this.id = 0;
            }
        }
        if (dict.has("company")) {
            this.company = dict.getInt("company");
            if (dict.isNull("company")) {
                this.company = 0;
            }
        }

        if (dict.has("name")) {
            this.name = dict.getString("name");
            if (dict.isNull("name")) {
                this.name = "";
            }
        }
        if (dict.has("key")) {
            this.key = dict.getString("key");
            if (dict.isNull("key")) {
                this.key = "";
            }
        }

        if (dict.has("logo")) {
            this.logo = dict.getString("logo");
            if (dict.isNull("logo")) {
                this.logo = "";
            }
        }

        if (dict.has("details")) {
            this.details = dict.getString("details");
            if (dict.isNull("details")) {
                this.details = "";
            }
        }

        if (dict.has("color")) {
            this.color = dict.getString("color");
            if (dict.isNull("color")) {
                this.color = "";
            }
        }

        if(dict.has("images")){

            this.image = new Images(dict.getJSONObject("images"));

        }

        return this;
    }
}
