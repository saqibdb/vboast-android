package com.vboost.pro.DBClasses;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.vboost.pro.VboostDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nabeel Hafeez on 9/2/2016.
 */
public class NewUser extends VboostDatabase {

    private String company;
    private String email;
    private String fullname;
    private String phone;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public NewUser(String _company,
                   String _email,
                   String _fullname,
                   String _phone) {

        this.setCompany(_company);
        this.setEmail(_email);
        this.setFullname(_fullname);
        this.setPhone(_phone);
    }

    private static NewUser eventFromStatement(Cursor cursor) {
        if (cursor != null) {
            try {
                if (cursor.getCount() == 0) {
                    return null;
                }

                String company = cursor.getString(0);
                String email = cursor.getString(1);
                String fullname = cursor.getString(2);
                String phone = cursor.getString(3);
                NewUser newUser = new NewUser(company, email, fullname, phone);
                return newUser;
            } catch (Exception e) {
                Log.e("TAG", "Error in cursor memory allocation: " + e.toString());
                return null;
            }
        } else {
            return null;
        }
    }

    public static List<NewUser> getAll() {
        String selectQuery = "SELECT company, email, fullname, phone FROM NewUser";
        Log.d("SELECT", selectQuery);
        Cursor c = selectDataForQuery(selectQuery);

        ArrayList<NewUser> newUsers = new ArrayList<NewUser>();
        NewUser newUser;
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    newUser = eventFromStatement(c);
                    newUsers.add(newUser);
                } while (c.moveToNext());
            }
            c.close();
        }
        return newUsers;
    }

    private static List<NewUser> findAllByCursor(Cursor c) {
        ArrayList<NewUser> newUsers = new ArrayList<NewUser>();
        NewUser newUser;
        if (c != null) {

            if (c.moveToFirst()) {
                do {
                    newUser = eventFromStatement(c);
                    newUsers.add(newUser);
                } while (c.moveToNext());
            }
            c.close();
        }
        return newUsers;
    }


    private static final String table = "NewUser";

    public boolean saveToDb() {

                ContentValues values = new ContentValues();
                values.put("company", this.company);
                values.put("email", this.email);
                values.put("fullname", this.fullname);
                values.put("phone", this.phone);

                return (insertDataForQuery(table, values) != -1);

    }

    public static void destroy() {
        sqLiteOpenHelper.getWritableDatabase().execSQL(String.format("DELETE FROM NewUser"));
    }
}
