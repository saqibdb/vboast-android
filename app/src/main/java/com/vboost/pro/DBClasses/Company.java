package com.vboost.pro.DBClasses;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.vboost.pro.VboostDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nabeel Hafeez on 9/2/2016.
 */
public class Company extends VboostDatabase {

    private int companyID;
    private String help;
    private String key;
    private String logo;
    private String name;
    private String status;
    private String terms;
    private String companyOwner;


    public int getCompanyID() {
        return companyID;
    }

    public void setCompanyID(int companyID) {
        this.companyID = companyID;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getCompanyOwner() {
        return companyOwner;
    }

    public void setCompanyOwner(String companyOwner) {
        this.companyOwner = companyOwner;
    }


    public Company(int _companyID,
                   String _help,
                   String _key,
                   String _logo,
                   String _name,
                   String _status,
                   String _terms,
                   String _companyOwner) {

        this.setCompanyID(_companyID);
        this.setHelp(_help);
        this.setKey(_key);
        this.setLogo(_logo);
        this.setName(_name);
        this.setStatus(_status);
        this.setTerms(_terms);
        this.setCompanyOwner(_companyOwner);

    }

    private static Company eventFromStatement(Cursor cursor) {
        if (cursor != null) {
            try {
                if (cursor.getCount() == 0) {
                    return null;
                }

                int companyID = cursor.getInt(0);
                String help = cursor.getString(1);
                String key = cursor.getString(2);
                String logo = cursor.getString(3);
                String name = cursor.getString(4);
                String status = cursor.getString(5);
                String terms = cursor.getString(6);
                String companyOwner = cursor.getString(7);
                Company company = new Company(companyID, help, key, logo, name, status, terms, companyOwner);
                return company;
            } catch (Exception e) {
                Log.e("TAG", "Error in cursor memory allocation: " + e.toString());
                return null;
            }
        } else {
            return null;
        }
    }

    public static List<Company> getAll() {
        String selectQuery = "SELECT companyID, help, key, logo, name, status, terms, companyOwner FROM Company";
        Log.d("SELECT", selectQuery);
        Cursor c = selectDataForQuery(selectQuery);

        ArrayList<Company> companies = new ArrayList<Company>();
        Company company;
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    company = eventFromStatement(c);
                    companies.add(company);
                } while (c.moveToNext());
            }
            c.close();
        }
        return companies;
    }

    private static List<Company> findAllByCursor(Cursor c) {
        ArrayList<Company> companies = new ArrayList<Company>();
        Company company;
        if (c != null) {

            if (c.moveToFirst()) {
                do {
                    company = eventFromStatement(c);
                    companies.add(company);
                } while (c.moveToNext());
            }
            c.close();
        }
        return companies;
    }

    public static Company findCompanyById(int companyId) {
        String selectQuery = "SELECT companyID, help, key, logo, name, status, terms, companyOwner FROM Company WHERE companyID = " + companyId;
        Log.d("SELECT", selectQuery);
        Cursor c = selectDataForQuery(selectQuery);
        if (c != null) {
            if (c.getCount() > 0) {
                Company company = eventFromStatement(c);
                c.close();
                return company;
            }
            c.close();
        }
        return null;
    }

    private static final String table = "Company";

    public boolean saveToDb() {

                ContentValues values = new ContentValues();
                values.put("companyID", this.companyID);
                values.put("help", this.help);
                values.put("key", this.key);
                values.put("logo", this.logo);
                values.put("name", this.name);
                values.put("status", this.status);
                values.put("terms", this.terms);
                values.put("companyOwner", this.companyOwner);

                return (insertDataForQuery(table, values) != -1);

    }

    public static void destroy() {
        sqLiteOpenHelper.getWritableDatabase().execSQL(String.format("DELETE FROM Company"));
    }


}
