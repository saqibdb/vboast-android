package com.vboost.pro.DBClasses;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

/**
 * Created by Nabeel Hafeez on 9/15/2016.
 */
public class ContactDataSave extends AsyncTask<String, String, String> {

    String company;
    int contact_id;
    String email;
    String is_active;
    String name;
    String phone;
    String photo;
    String sync_status;
    String title;
    String type;
    String contactOwner;


    public ContactDataSave(String _company,
                           int _contact_id,
                           String _email,
                           String _is_active,
                           String _name,
                           String _phone,
                           String _photo,
                           String _sync_status,
                           String _title,
                           String _type,
                           String _contactOwner){

        this.company = _company;
        this.contact_id = _contact_id;
        this.email = _email;
        this.is_active =_is_active;
        this.name = _name;
        this.phone = _phone;
        this.photo = _photo;
        this.sync_status = _sync_status;
        this.title = _title;
        this.type = _type;
        this.contactOwner = _contactOwner;


    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(String... strings) {


        Contact contact = new Contact(company,contact_id, email,is_active, name, phone, photo, sync_status, title, type, contactOwner);
        contact.saveToDb();

        return null;
    }

    @Override
    protected void onPostExecute(String unused) {



    }

    public String imageStringFormat(String path){

        Bitmap bitmapOrg = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 100, bao);
        byte [] ba = bao.toByteArray();
        String image= Base64.encodeToString(ba,Base64.DEFAULT);

        return image;
    }

}
