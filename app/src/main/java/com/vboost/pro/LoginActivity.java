package com.vboost.pro;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnItemClickListener;
import com.vboost.pro.DBClasses.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Nabeel Hafeez on 7/27/2016.
 */
public class LoginActivity extends AppCompatActivity implements OnItemClickListener {

    JSONObject jsonObject;
    EditText usernameFeild;
    EditText passwordField;
    Button loginNextButton;

    AlertView mAlertViewExt;
    AlertView networkErrorAlert;

    String userName;
    String password;
    String error ;

    private final String USER_AGENT = "Mozilla/5.0";
    private ProgressDialog progress;

    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Username = "usernameKey";
    public static final String Password = "passwordKey";
    public static final String Token = "tokenKey";
    SharedPreferences sharedpreferences;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        usernameFeild = (EditText) findViewById(R.id.login_company_identifier_edit_text);
        passwordField = (EditText) findViewById(R.id.login_company_paswword_edit_text);
        loginNextButton = (Button) findViewById(R.id.login_next_button);
        loginNextButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#FBA919"), 0x00000000));
        usernameFeild.setSingleLine(true);

        loginNextButton.setAllCaps(false);

        mAlertViewExt = new AlertView("Error", "We're sorry, company identifier and password are incorrect", "Try Again", null, new String[]{"Request Login"}, this, AlertView.Style.Alert, this);
        networkErrorAlert = new AlertView("Network Error", "Please ensure a strong internet\nconnectivity and try again", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this);

        userName = sharedpreferences.getString(Username, null);
        password = sharedpreferences.getString(Password, null);
        if (userName !=null && password !=null) {

            usernameFeild.setText(userName);
            passwordField.setText(password);

            if (isOnline(LoginActivity.this)) {

                User.destroy();
                new loginVboost(this).execute();
            } else {
                user = User.findByUsername("'" + usernameFeild.getText().toString() + "'");
                if (user != null) {
                    if (user.getUsername().equals(usernameFeild.getText().toString()) && user.getPassword().equals(passwordField.getText().toString())) {

                        Intent splashIntent = new Intent(LoginActivity.this, ManageContatcsAvtivity.class);
                        startActivity(splashIntent);
                        finish();
                    }else{
                        mAlertViewExt.show();
                    }
                } else {

                    networkErrorAlert.show();

                }

            }
        }
    }

    public void attemptLogin(View view) {

        userName = usernameFeild.getText().toString();
        password = passwordField.getText().toString();

        if (userName.isEmpty() || userName.equals(null) || userName == null) {

            new AlertView(null, "\nPlease enter the company identifier.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();

        } else if (password.isEmpty()  || password.equals(null) || password == null) {

            new AlertView(null, "\nPlease enter the company password.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();


        } else {

            new loginVboost(this).execute();
        }
    }

    @Override
    public void onItemClick(Object o, int position) {

        if (o == mAlertViewExt && position != AlertView.CANCELPOSITION) {

            Intent requestIntent = new Intent(LoginActivity.this, RequestLoginActivity.class);
            startActivity(requestIntent);

            return;
        }

    }

    private class loginVboost extends AsyncTask<String, Void, Void> {

        private final Context context;

        public loginVboost(Context c) {
            this.context = c;
        }

        protected void onPreExecute() {
            progress = new ProgressDialog(this.context);
            progress.setMessage("Login Please wait...");
            progress.show();
        }

        @Override
        protected Void doInBackground(String... params) {


            try {

                URL url = new URL("http://vboostoffice.com/api/v1/auth/login");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                //add reuqest header
                connection.setRequestMethod("POST");
                connection.setRequestProperty("User-Agent", USER_AGENT);
                connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

                String urlParameters = "username=" + userName + "&password=" + password;

                // Send post request
                connection.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                final int responseCode = connection.getResponseCode();
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + urlParameters);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String inputLine;
                final StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                //print result
                System.out.println("Login Response:  "+response.toString());

                LoginActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        if (responseCode == 200) {
                            try {
                                String responseString = response.toString();
                                System.out.println(response.toString());
                                /**
                                 * Parse JSON response to Gson library
                                 */
                                jsonObject = new JSONObject(responseString);
                                String token = jsonObject.getString("auth_token");
                                Log.e("token", token);

                                SharedPreferences.Editor editor = sharedpreferences.edit();

                                editor.putString(Username, userName);
                                editor.putString(Password, password);
                                editor.putString(Token, token);
                                editor.commit();

                                User user = new User(token, password, userName, null, null, null, null);
                                user.saveToDb(LoginActivity.this);


                                progress.dismiss();
                                Intent splashIntent = new Intent(LoginActivity.this, ManageContatcsAvtivity.class);
                                startActivity(splashIntent);
                                finish();

                            } catch (final JSONException e) {
                                e.printStackTrace();
                                progress.dismiss();
                            }
                        } else if (responseCode == 400) {

                            String responseString = response.toString();
                            System.out.println(response.toString());

                            try {
                                jsonObject = new JSONObject(responseString);
                                error = jsonObject.getString("non_field_errors");

                                Log.e("non_field_errors", error);



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progress.dismiss();

                                        mAlertViewExt.show();
                                }
                            });

                        }
                    }
                });

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                Log.e("Mal exc", "" + e);
                progress.dismiss();
                e.printStackTrace();
            } catch (final IOException e) {
                // TODO Auto-generated catch block
                Log.e("IO exc", "" + e.getMessage() + ":  " + e.getCause());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progress.dismiss();
                        if(e.getMessage().contains("Unable to resolve host")) {
                            networkErrorAlert.show();
                        }else{
                            mAlertViewExt.show();
                        }
                    }
                });
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute() {
            //progress.dismiss();
        }

    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }


}
