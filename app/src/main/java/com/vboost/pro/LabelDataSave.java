package pro.saqibdb.vboost.pro;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;

import com.vboost.pro.DBClasses.DataLabels;

import java.io.ByteArrayOutputStream;

/**
 * Created by Nabeel Hafeez on 9/15/2016.
 */
public class LabelDataSave extends AsyncTask<String, String, String> {

    DataLabels dataLabels;
    String image;
    int imageSequence;
    String name;
    String title;
    String campaignOwner;
    String labelPreference;


    public LabelDataSave(String _image,
                         int _imageSequence,
                         String _name,
                         String _title,
                         String _campaignOwner,
                         String _labelPreference){

        this.image = _image;
        this.imageSequence = _imageSequence;
        this.name = _name;
        this.title =_title;
        this.campaignOwner = _campaignOwner;
        this.labelPreference = _labelPreference;

    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(String... strings) {

        String imageAsString = imageStringFormat(image);
        dataLabels = new DataLabels(imageAsString, imageSequence, name, title, campaignOwner, labelPreference);
        dataLabels.saveToDb();

        return null;
    }

    @Override
    protected void onPostExecute(String unused) {



    }

    public String imageStringFormat(String path){

        Bitmap bitmapOrg = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 100, bao);
        byte [] ba = bao.toByteArray();
        String image= Base64.encodeToString(ba,Base64.DEFAULT);

        return image;
    }

}
