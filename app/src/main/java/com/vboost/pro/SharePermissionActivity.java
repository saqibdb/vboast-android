package com.vboost.pro;

import android.app.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import android.os.Build;
import android.os.Bundle;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;

import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;


import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;
import com.vboost.pro.DBClasses.Company;
import com.vboost.pro.DBClasses.DataLabels;
import com.vboost.pro.DBClasses.Preference;
import com.vboost.pro.event.NetworkRequestFailedEvent;
import com.vboost.pro.event.PhotoUploadRequestEvent;
import com.vboost.pro.event.PhotoUploadResponseEvent;
import com.vboost.pro.model.Campaigns;
import com.vboost.pro.model.ManageContacts;
import com.vboost.pro.model.PhotoAndName;
import com.vboost.pro.service.ApiClient;

import java.io.ByteArrayOutputStream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Nabeel Hafeez on 8/12/2016.
 */
public class SharePermissionActivity extends AppCompatActivity {

    ArrayList<ManageContacts> contactsList;
    ArrayList<Campaigns> campaignsArray;
    Campaigns campaign;
    String[] contactsName;
    boolean singleCampaign;

    Button givesPermission;
    Button notGivesPermission;

    //String[] imageArray;
    String name;
    String deliveryMethod;
    String flag;
    int campaignId;
    int contactId;
    String email;
    String phone;

    List<DataLabels> imagesAndLabels;
    //List<byte[]> fileList = new ArrayList<>();
    //ArrayList<String> imagesTitle = new ArrayList<>();

    ArrayList<PhotoAndName> photoAndNameArrayList = new ArrayList<>();

    private ProgressDialog progress;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Token = "tokenKey";
    SharedPreferences sharedpreferences;

    String userCredentials;

    ApiClient client;
    ArrayList<Uri> browsedUris = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_share_permission);

        List<Company> company =  Company.getAll();

        ImageView companyLogo = (ImageView) findViewById(R.id.logo2);

        Picasso.with(this)
                .load(company.get(0).getLogo())
                .placeholder(R.drawable.logo2)
                .error(R.drawable.logo2)
                .into(companyLogo);

        progress = new ProgressDialog(this);
        progress.setMessage("Please wait...");

        givesPermission = (Button) findViewById(R.id.gives_permission_button);
        notGivesPermission = (Button) findViewById(R.id.not_gives_permission_button);
        givesPermission.setAllCaps(false);
        notGivesPermission.setAllCaps(false);

        imagesAndLabels = DataLabels.getAll();

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        userCredentials = sharedpreferences.getString(Token, null);

        client = ApiClient.getInstance(getApplicationContext());

        givesPermission.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"), 0x00000000));
        notGivesPermission.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#8ec63e"), 0x00000000));

        name = getIntent().getStringExtra("name");
        deliveryMethod = getIntent().getStringExtra("deliverMethod");
        campaignId = getIntent().getIntExtra("campaignID", -1);
        contactId = getIntent().getIntExtra("contactID", -1);
        flag = getIntent().getStringExtra("deliveryFlag");
        contactsName = getIntent().getStringArrayExtra("contactsName");
        contactsList = (ArrayList<ManageContacts>) getIntent().getSerializableExtra("contacts");
        campaign = (Campaigns) getIntent().getSerializableExtra("campaignrow");
        campaignsArray = (ArrayList<Campaigns>) getIntent().getSerializableExtra("campaignArray");
        singleCampaign = getIntent().getBooleanExtra("singleCampaign", false);

        for(DataLabels dataLabels : imagesAndLabels){

            Uri myUriImagePath = Uri.parse(dataLabels.getImage());
            browsedUris.add(myUriImagePath);
            String imageTitle = dataLabels.getTitle();
            try {
            InputStream imageStream = null;
            try {
                imageStream = getContentResolver().openInputStream(myUriImagePath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            Bitmap compressBitmap = BitmapFactory.decodeStream(imageStream);

            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            compressBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outStream);
            PhotoAndName photoAndName = new PhotoAndName();
                photoAndName.setPhotos(outStream.toByteArray());
                photoAndName.setName(imageTitle);
                photoAndNameArrayList.add(photoAndName);
            } catch (Exception e) {
                throw new IllegalStateException("Could not read file", e);
            }

        }

/*
        for (int i = 0; i < imagesAndLabels.size(); i++) {
            DataLabels dataLabels = imagesAndLabels.get(i);
            Uri myUriImagePath = Uri.parse(dataLabels.getImage());
            imagesTitle.add(dataLabels.getTitle());
            browsedUris.add(myUriImagePath);
        }

        try {
            for (Uri uri : browsedUris) {
                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                Bitmap compressBitmap = BitmapFactory.decodeStream(imageStream);

                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                compressBitmap.compress(Bitmap.CompressFormat.JPEG, 20, outStream);
                fileList.add(outStream.toByteArray());
                //fileList.add(IOUtils.toByteArray(this.getContentResolver().openInputStream(uri)));
            }
        } catch (Exception e) {
            throw new IllegalStateException("Could not read file", e);
        }*/

        if (flag.equals("email")) {
            email = deliveryMethod;
            phone = null;
        }else{
            phone = deliveryMethod;
            email = null;
        }

        givesPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yesPermission();
            }
        });

        notGivesPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.show();
                noPermission();
            }
        });

    }

    public void yesPermission(){

        Intent yesIntent = new Intent(SharePermissionActivity.this, SignatureActivity.class);
        yesIntent.putExtra("name", name);
        yesIntent.putExtra("deliverMethod", deliveryMethod);
        yesIntent.putExtra("campaignID", campaignId);
        yesIntent.putExtra("contactID", contactId);
        yesIntent.putExtra("deliveryFlag", flag);
       // startActivity(yesIntent);
        startActivityForResult(yesIntent, 1);
    }


    public void noPermission() {

        Preference preference = new Preference(campaignId, contactId, email, name, "0", phone, null, null, null, null, null, null);
        preference.saveToDb();



        if (isOnline(this)) {

            client.getBus().post(new PhotoUploadRequestEvent(photoAndNameArrayList,  campaignId, contactId, name, email, 0, phone, ""));

        } else {

            Intent intent = new Intent("my.action.string");
            intent.putExtra("userCredentials", userCredentials);
            sendBroadcast(intent);
            progress.dismiss();
            ViewDialog alert = new ViewDialog();
            alert.showDialog(SharePermissionActivity.this);

        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        client.getBus().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        client.getBus().unregister(this);
    }

    @Subscribe
    public void onPhotoUploadFinished(PhotoUploadResponseEvent event) {

        if (event.isSuccess()) {
            if(!browsedUris.isEmpty()) {
                for (Uri uri : browsedUris) {
                    deleteImage(uri);
                }
            }
            DataLabels.destroy();
            Preference.destroy();
            if(progress !=null && progress.isShowing())
                progress.dismiss();
            ViewDialog alert = new ViewDialog();
            alert.showDialog(SharePermissionActivity.this);
           //Toast.makeText(this, "Uploaded successfully", Toast.LENGTH_SHORT).show();
        } else
            progress.dismiss();
           //Toast.makeText(this, "Failed to upload", Toast.LENGTH_SHORT).show();

        if (event.getMessage() != null)
            progress.dismiss();
            System.out.println("Data not Send Becuase:  "+event.getMessage() +" Cause"+event.isSuccess());//logText.setText(event.getMessage());
    }

    @Subscribe
    public void onNetworkRequestFailed(NetworkRequestFailedEvent event) {
        if(progress !=null && progress.isShowing())
        progress.dismiss();
        if(event.getCause().getMessage().contains("timeout") ||  event.getCause().getMessage().contains("connect timed out")){
            ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
            viewRetryDialog.showDialog(SharePermissionActivity.this);
        }else {
            Intent intent = new Intent("my.action.string");
            intent.putExtra("userCredentials", userCredentials);
            sendBroadcast(intent);
            ViewDialog alert = new ViewDialog();
            alert.showDialog(SharePermissionActivity.this);
        }
       // Toast.makeText(this, "Please make sure network is turned on", Toast.LENGTH_SHORT).show();
    }

    public void sharePermissionBack(View view) {


        finish();
    }

    public class ViewDialog {

        public void showDialog(Activity activity){
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.permission_ok_alert);

            Button dialogButton = (Button) dialog.findViewById(R.id.done_button_dialog);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if(singleCampaign){

                        Intent campaignsDataIntent = new Intent(SharePermissionActivity.this,CampaignsDataActivity.class);
                        campaignsDataIntent.putExtra("contactsName",contactsName);
                        campaignsDataIntent.putExtra("campaignrow",campaign);
                        campaignsDataIntent.putExtra("contacts",contactsList);
                        campaignsDataIntent.putExtra("campaignArray", campaignsArray);
                        startActivity(campaignsDataIntent);
                        finish();

                    }else {
                        finish();
                    }
                }
            });

            dialog.show();

        }
    }

    public class ViewRetryDialog {

        public void showDialog(Activity activity){
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.retry_alert);

            Button dialogButton = (Button) dialog.findViewById(R.id.retry_button_dialog);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    client.getBus().post(new PhotoUploadRequestEvent(photoAndNameArrayList, campaignId, contactId, name, email, 0, phone, ""));
                }
            });

            dialog.show();

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){

                ViewDialog alert = new ViewDialog();
                alert.showDialog(SharePermissionActivity.this);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }//onActivityResult


 /*   public String imageStringFormat(String path) {

        Bitmap bitmapOrg = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        byte[] byteArray = stream.toByteArray();
        String imageString = Base64.encodeToString(byteArray, 0);

        return imageString;
    }*/
/*
    public String createJson() {

        try {


            JSONArray jsonArrayTo = new JSONArray();

            for (int i = 0; i < imagesAndLabels.size(); i++) {
                DataLabels dataLabels = imagesAndLabels.get(i);
                // Log.d("Image String", imageStringFormat(dataLabels.getImage()));
                JSONObject jsonObjectTo = new JSONObject();
                String image = dataLabels.getImage();
                String newString = image.replaceAll("\n", "");
                writeToSDFile(newString, "ImageNo"+i);
                jsonObjectTo.put("image", newString);
                jsonObjectTo.put("name", dataLabels.getName());
                jsonArrayTo.put(jsonObjectTo);
            }


            System.out.println("output:   "+ jsonArrayTo.toString(2));
            return jsonArrayTo.toString(2);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class ShareNoPermission extends AsyncTask<String, Void, Void> {

        private final Context context;

        public ShareNoPermission(Context c) {
            this.context = c;
        }

        protected void onPreExecute() {
            progress = new ProgressDialog(this.context);
            progress.setMessage("Loading");
            progress.show();
        }

        @Override
        protected Void doInBackground(String... params) {

           // postmanFunction();
            try {

                URL url = new URL("http://vboostoffice.com/api/v1/packages/");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                String finalImageJson = imagesJson.replaceAll("\\\\", "");
                String urlParameters = "campaign=" + campaignId +
                        "&contact=" + contactId +
                        "&recipient_name=" + name +
                        "&recipient_phone=" + deliveryMethod +
                        "&recipient_permission=0"+
                        "&images="+theString;

                String tokenAuth = "Token " + userCredentials;
                connection.setRequestProperty("Authorization", tokenAuth);

                //connection.setDoInput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("User-Agent", USER_AGENT+android.os.Build.VERSION.SDK_INT);
                connection.setRequestProperty("charset", "utf-8");
                connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
                connection.setUseCaches(false);


                // Send post request
                connection.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.write(urlParameters.getBytes());
                wr.flush();
                wr.close();

                final int responseCode = connection.getResponseCode();
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + urlParameters);
                System.out.println("Response Code : " + responseCode);

                try {

                    BufferedReader reader = null;

                    if(connection.getResponseCode() == 201)
                    {
                        reader = new BufferedReader(new
                                InputStreamReader(connection.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = reader.readLine()) != null) {
                            response.append(inputLine);
                        }
                        reader.close();
                        System.out.println(response.toString());
                    }
                    else
                    {
                        reader = new BufferedReader(new
                                InputStreamReader(connection.getErrorStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = reader.readLine()) != null) {
                            response.append(inputLine);
                        }
                        reader.close();
                        System.out.println(response.toString());
                    }

                    // BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));


                    //print result

                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Catch Error:   "+ e.getLocalizedMessage());
                }


                SharePermissionActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        if (responseCode == 201) {
                            progress.dismiss();
                            DataLabels.destroy();
                            Preference.destroy();
                            ViewDialog alert = new ViewDialog();
                            alert.showDialog(SharePermissionActivity.this);

                        }else if(responseCode == 500){
//                            DataLabels.destroy();
//                            Preference.destroy();
                            Toast.makeText(SharePermissionActivity.this, "Sorry, unexpected error has occured. Our administrators are already notified and shall do their best to solve it.", Toast.LENGTH_LONG).show();
                            progress.dismiss();

                        }else {
//                            DataLabels.destroy();
//                            Preference.destroy();
                            Toast.makeText(SharePermissionActivity.this, "Data not send", Toast.LENGTH_LONG).show();
                            progress.dismiss();
                        }

                    }
                });

            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute() {
            progress.dismiss();
        }

    }*/


    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }

    public void deleteImage(Uri uri) {
        //String file_dj_path = Environment.getExternalStorageDirectory() + "/ECP_Screenshots/abc.jpg";
        File fdelete = new File(uri.getPath());
        if (fdelete.exists()) {
            if (fdelete.delete()) {

                if(fdelete.exists()){
                    try {
                        fdelete.getCanonicalFile().delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(fdelete.exists()){
                        getApplicationContext().deleteFile(fdelete.getName());
                    }
                }
                Log.e("-->", "file Deleted :" + uri.getPath());
                callBroadCast(uri.getPath(), true);
            } else {
                Log.e("-->", "file not Deleted :" + uri.getPath());
            }
        }
    }

    public void callBroadCast(String path, final boolean isDelete) {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(this, new String[]{path}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    if (isDelete) {
                        if (uri != null) {
                            getApplicationContext().getContentResolver().delete(uri,
                                    null, null);
                        }
                    }
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

}
