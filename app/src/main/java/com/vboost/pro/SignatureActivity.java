package com.vboost.pro;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnDismissListener;
import com.bigkoo.alertview.OnItemClickListener;
import com.kyanogen.signatureview.SignatureView;

import com.squareup.otto.Subscribe;
import com.vboost.pro.DBClasses.Company;
import com.vboost.pro.DBClasses.DataLabels;
import com.vboost.pro.DBClasses.Preference;
import com.vboost.pro.event.NetworkRequestFailedEvent;
import com.vboost.pro.event.PhotoUploadRequestEvent;
import com.vboost.pro.event.PhotoUploadResponseEvent;
import com.vboost.pro.model.PhotoAndName;
import com.vboost.pro.service.ApiClient;

import java.io.ByteArrayOutputStream;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.List;


public class SignatureActivity extends AppCompatActivity implements OnItemClickListener, OnDismissListener {

    private SignatureView signatureView;
    Button clearButton;
    Button okButton;
    TextView signatureText;
    ImageButton acceptTermCondition;
    TextView hardCodedTerms;
    LinearLayout termsCheckBoxLayout;

    boolean accept = false;
    //globally
    private boolean isSignatured = false;

   // String[] imageArray;
    String name;
    String deliveryMethod;
    String flag;
    int campaignId;
    int contactId;
    File file;
    String email;
    String phone;

    private ProgressDialog progress;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Token = "tokenKey";
    SharedPreferences sharedpreferences;

    String userCredentials;
    String signature;
    List<DataLabels> imagesAndLabels;

    ApiClient client;
    ArrayList<Uri> browsedUris = new ArrayList<>();
    ArrayList<PhotoAndName> photoAndNameArrayList  = new ArrayList<>();
    //ArrayList<String> imagesTitle = new ArrayList<>();
    //List<byte[]> fileList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int orientation =  ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        setRequestedOrientation(orientation);
        setContentView(R.layout.activity_signature);//see xml layout

        initializeUI();

    }

    public void initializeUI(){

        progress = new ProgressDialog(this);
        progress.setMessage("Please wait...");

        List<Company> company =  Company.getAll();

        signatureView = (SignatureView) findViewById(R.id.signature_view);

        clearButton = (Button) findViewById(R.id.signature_clear_button);
        okButton = (Button) findViewById(R.id.signatur_ok_button);
        acceptTermCondition = (ImageButton) findViewById(R.id.accept_terms_and_conditions);
        hardCodedTerms = (TextView) findViewById(R.id.terms_hard_code_text_view);
        signatureText = (TextView) findViewById(R.id.signature_hint_text);
        termsCheckBoxLayout = (LinearLayout) findViewById(R.id.terms_check_box_layout);
        String termsText = company.get(0).getTerms();
        if(termsText.equals("") || termsText == null){
            accept = true;
            termsCheckBoxLayout.setVisibility(View.INVISIBLE);
            hardCodedTerms.setVisibility(View.VISIBLE);
        }else {
            hardCodedTerms.setVisibility(View.INVISIBLE);
            termsCheckBoxLayout.setVisibility(View.VISIBLE);
            acceptTermCondition.setImageResource(0);
            acceptTermCondition.setBackground(getResources().getDrawable(R.drawable.r_un));
        }

        imagesAndLabels = DataLabels.getAll();

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        userCredentials = sharedpreferences.getString(Token, null);

        client = ApiClient.getInstance(getApplicationContext());

        /*try {
            theString2 = IOUtils.toString(new FileInputStream(new File("/storage/emulated/0/Download/JsonArray.txt")), "UTF-8");
            newsignature = IOUtils.toString(new FileInputStream(new File("/storage/emulated/0/Download/signature.txt")), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        signatureView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                signatureText.setVisibility(View.INVISIBLE);
                isSignatured = true;
                return false;
            }
        });






        acceptTermCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (accept) {
                    accept = false;
                    acceptTermCondition.setImageResource(0);
                    acceptTermCondition.setBackground(getResources().getDrawable(R.drawable.r_un));
                } else {
                    accept = true;
                    acceptTermCondition.setImageResource(0);
                    acceptTermCondition.setBackground(getResources().getDrawable(R.drawable.r_sel));

                }

            }
        });

        clearButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#FBA919"), 0x00000000));
        okButton.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#FBA919"), 0x00000000));

        name = getIntent().getStringExtra("name");
        deliveryMethod = getIntent().getStringExtra("deliverMethod");
        campaignId = getIntent().getIntExtra("campaignID", -1);
        contactId = getIntent().getIntExtra("contactID", -1);
        flag = getIntent().getStringExtra("deliveryFlag");

        if (flag.equals("email")) {
            email = deliveryMethod;
            phone = null;
        } else {
            phone = deliveryMethod;
            email = null;
        }

        for(DataLabels dataLabels : imagesAndLabels){

            Uri myUriImagePath = Uri.parse(dataLabels.getImage());
            browsedUris.add(myUriImagePath);
            String imageTitle = dataLabels.getTitle();
            try {
                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(myUriImagePath);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                Bitmap compressBitmap = BitmapFactory.decodeStream(imageStream);

                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                compressBitmap.compress(Bitmap.CompressFormat.JPEG, 10, outStream);
                PhotoAndName photoAndName = new PhotoAndName();
                photoAndName.setPhotos(outStream.toByteArray());
                photoAndName.setName(imageTitle);
                photoAndNameArrayList.add(photoAndName);
            } catch (Exception e) {
                throw new IllegalStateException("Could not read file", e);
            }

        }

       /* for (int i = 0; i < imagesAndLabels.size(); i++) {
            DataLabels dataLabels = imagesAndLabels.get(i);
            Uri myUriImagePath = Uri.parse(dataLabels.getImage());
            imagesTitle.add(dataLabels.getTitle());
            browsedUris.add(myUriImagePath);
        }

        try {
            for (Uri uri : browsedUris) {
                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                Bitmap compressBitmap = BitmapFactory.decodeStream(imageStream);

                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                compressBitmap.compress(Bitmap.CompressFormat.JPEG, 20, outStream);
                fileList.add(outStream.toByteArray());
                // fileList.add(IOUtils.toByteArray(this.getContentResolver().openInputStream(uri)));
            }
        } catch (Exception e) {
            throw new IllegalStateException("Could not read file", e);
        }*/

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                okSignature();

            }
        });
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        int orientation =  ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        setRequestedOrientation(orientation);

        setContentView(R.layout.activity_signature);//see xml layout

        initializeUI();
    }

    public void openTermsandConditons(View view) {

        Intent termIntent = new Intent(SignatureActivity.this, TermandCondtions.class);
        startActivity(termIntent);


    }


    public void clearSignature(View view) {

        signatureView.clearCanvas();//Clear SignatureView
        signatureText.setVisibility(View.VISIBLE);
        isSignatured = false;
        Toast.makeText(getApplicationContext(),
                "Clear canvas", Toast.LENGTH_SHORT).show();


    }

    public void okSignature() {

        if(isSignatured){
            //do next process

        if (accept) {
            progress.show();
            File sdCard = Environment.getExternalStorageDirectory();
            File directory = new File(sdCard.getAbsolutePath() + "/Vboost");
//            File directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            file = new File(directory, System.currentTimeMillis() + ".jpg");
            FileOutputStream out = null;
            Bitmap bitmap = signatureView.getSignatureBitmap();
            try {
                out = new FileOutputStream(file);
                if (bitmap != null) {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 20, out);
                } else {
                    throw new FileNotFoundException();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.flush();
                        out.close();

                        if (bitmap != null) {
//                        Toast.makeText(getApplicationContext(),
//                                "Image saved successfully at "+ file.getPath(),Toast.LENGTH_LONG).show();
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                                new MyMediaScanner(this, file);
                            } else {
                                ArrayList<String> toBeScanned = new ArrayList<String>();
                                toBeScanned.add(file.getAbsolutePath());
                                String[] toBeScannedStr = new String[toBeScanned.size()];
                                toBeScannedStr = toBeScanned.toArray(toBeScannedStr);
                                MediaScannerConnection.scanFile(this, toBeScannedStr, null, null);
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (file.getAbsolutePath() == null) {
                signature = "";
            } else {
                signature = imageStringFormat(file.getAbsolutePath());
            }

            Preference preference = new Preference(campaignId, contactId, email, name, "1", phone, signature, null, null, null, null, null);
            preference.saveToDb();

            if (isOnline(this)) {

                client.getBus().post(new PhotoUploadRequestEvent(photoAndNameArrayList,  campaignId, contactId, name, email, 1, phone, signature));

            } else {

                Intent intent = new Intent("my.action.string");
                intent.putExtra("userCredentials", userCredentials);
                sendBroadcast(intent);
                progress.dismiss();
                Intent intentback = new Intent();
                setResult(RESULT_OK, intentback);
                finish();

            }

        } else {
            progress.dismiss();
            new AlertView(null, "\nYou must agree to the terms and conditions.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();
            //Toast.makeText(SignatureActivity.this, "Please accept term and conditions first.", Toast.LENGTH_LONG).show();
        }
        }else{
            new AlertView(null, "\nPlease enter your signature.", null, null, new String[]{"OK"}, this, AlertView.Style.Alert, this).show();
            //intimate to sign
        }

    }

    public void signatureBack(View view) {

        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public void onDismiss(Object o) {

    }

    @Override
    public void onItemClick(Object o, int position) {

    }

    public class MyMediaScanner implements
            MediaScannerConnection.MediaScannerConnectionClient {

        private MediaScannerConnection mSC;
        private File file;

        public MyMediaScanner(Context context, File file) {
            this.file = file;
            mSC = new MediaScannerConnection(context, this);
            mSC.connect();
        }

        @Override
        public void onMediaScannerConnected() {
            mSC.scanFile(file.getAbsolutePath(), null);
        }

        @Override
        public void onScanCompleted(String path, Uri uri) {
            mSC.disconnect();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        client.getBus().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        client.getBus().unregister(this);
    }

    @Subscribe
    public void onPhotoUploadFinished(PhotoUploadResponseEvent event) {

        if (event.isSuccess()) {
            if(!browsedUris.isEmpty()) {
                for (Uri uri : browsedUris) {
                    deleteImage(uri);
                }
            }
            if (file.exists()) {
                if (file.delete()) {

                    if (file.exists()) {
                        try {
                            file.getCanonicalFile().delete();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (file.exists()) {
                            getApplicationContext().deleteFile(file.getName());
                        }
                    }

                    callBroadCast(file.getAbsolutePath(), true);
                } else {

                }
            }

            DataLabels.destroy();
            Preference.destroy();
            progress.dismiss();
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
//            ViewDialog alert = new ViewDialog();
//            alert.showDialog(SignatureActivity.this);
            //Toast.makeText(this, "Uploaded successfully", Toast.LENGTH_SHORT).show();
        } else
            progress.dismiss();
        //Toast.makeText(this, "Failed to upload", Toast.LENGTH_SHORT).show();

        if (event.getMessage() != null)
            progress.dismiss();
        System.out.println(event.getMessage());//logText.setText(event.getMessage());
    }

    @Subscribe
    public void onNetworkRequestFailed(NetworkRequestFailedEvent event) {
        progress.dismiss();
        if ( event.getCause().getMessage().contains("timeout") || event.getCause().getMessage().contains("connect timed out")){
            ViewRetryDialog viewRetryDialog = new ViewRetryDialog();
            viewRetryDialog.showDialog(SignatureActivity.this);
        } else {
            Intent intent = new Intent("my.action.string");
            intent.putExtra("userCredentials", userCredentials);
            sendBroadcast(intent);
            if (file.exists()) {
                if (file.delete()) {

                    if (file.exists()) {
                        try {
                            file.getCanonicalFile().delete();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (file.exists()) {
                            getApplicationContext().deleteFile(file.getName());
                        }
                    }

                    callBroadCast(file.getAbsolutePath(), true);
                } else {

                }
            }

            progress.dismiss();
            Intent intentback = new Intent();
            setResult(RESULT_OK, intentback);
            finish();
        }
        // Toast.makeText(this, "Please make sure network is turned on", Toast.LENGTH_SHORT).show();
    }

    public void sharePermissionBack(View view) {


        finish();
    }



    public class ViewRetryDialog {

        public void showDialog(Activity activity) {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.retry_alert);

            Button dialogButton = (Button) dialog.findViewById(R.id.retry_button_dialog);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    client.getBus().post(new PhotoUploadRequestEvent(photoAndNameArrayList, campaignId, contactId, name, email, 1, phone, signature));
                }
            });

            dialog.show();

        }
    }


    public String imageStringFormat(String path) {

        Bitmap bitmapOrg = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 10, stream);
        byte[] byteArray = stream.toByteArray();
        String imageString = Base64.encodeToString(byteArray, 0);

        return imageString;
    }

    public void deleteImage(Uri uri) {
        //String file_dj_path = Environment.getExternalStorageDirectory() + "/ECP_Screenshots/abc.jpg";
        File fdelete = new File(uri.getPath());
        if (fdelete.exists()) {
            if (fdelete.delete()) {

                if (fdelete.exists()) {
                    try {
                        fdelete.getCanonicalFile().delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (fdelete.exists()) {
                        getApplicationContext().deleteFile(fdelete.getName());
                    }
                }
                Log.e("-->", "file Deleted :" + uri.getPath());
                callBroadCast(uri.getPath(), true);
            } else {
                Log.e("-->", "file not Deleted :" + uri.getPath());
            }
        }
    }

    public void callBroadCast(String path, final boolean isDelete) {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(this, new String[]{path}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    if (isDelete) {
                        if (uri != null) {
                            getApplicationContext().getContentResolver().delete(uri,
                                    null, null);
                        }
                    }
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());

    }

}