package com.vboost.pro.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * @author Samvel Abrahamyan
 */

public interface ImageService{
    @POST("packages/?format=json")
    @FormUrlEncoded
    Call<ResponseBody> uploadPhoto(
            @Field("campaign") Integer campaign,
            @Field("contact") Integer contact,
            @Field("recipient_name") String recipientName,
            @Field("recipient_email") String recipientEmail,
            @Field("recipient_permission") Integer permission,
            @Field("recipient_phone") String recipientPhone,
            @Field("recipient_signature") String signature,
            @Field("images") String images
    );
}
