package com.vboost.pro.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.vboost.pro.CampaignsDataActivity;
import com.vboost.pro.DBClasses.Campaign;
import com.vboost.pro.model.ManageContacts;
import com.vboost.pro.R;
import com.vboost.pro.model.Campaigns;

import java.util.ArrayList;

/**
 * Created by Nabeel Hafeez on 7/27/2016.
 */

public class CampaignsAdapter extends BaseAdapter{

    ArrayList<ManageContacts> contacts;
    ArrayList<Campaigns> campaigns;
    String[] contactsName;
    Context context;
    boolean network;

    private static LayoutInflater inflater = null;

    public CampaignsAdapter(Activity activity,ArrayList<Campaigns> _campaigns,ArrayList<ManageContacts> _contacts,String[] _contactsName,boolean _network) {
        // TODO Auto-generated constructor stub
        this.contacts = _contacts;
        this.campaigns = _campaigns;
        this.contactsName = _contactsName;
        this.network = _network;
        this.context = activity;

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return campaigns.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {

        Button selectCampaigns;

    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_item_campaigns,null);

        final Campaigns singleRow = campaigns.get(position);

        holder.selectCampaigns = (Button) rowView.findViewById(R.id.campaigns_list_item_button);
        holder.selectCampaigns.setAllCaps(false);

        holder.selectCampaigns.setText(singleRow.getName());
        holder.selectCampaigns.getBackground().setColorFilter(new LightingColorFilter(Color.parseColor("#" + singleRow.getColor()),0x00000000));
        if (network) {
            Campaign campaign = new Campaign(singleRow.getColor(),singleRow.getDetails(),singleRow.getId(),singleRow.getCompany(),singleRow.getImage().getDefault_count(),singleRow.getKey(),singleRow.getLogo(),singleRow.getImage().getMax_count(),singleRow.getImage().getMin_count(),singleRow.getName(),null,null,null);
            campaign.saveToDb();
        }
        holder.selectCampaigns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent campaignsDataIntent = new Intent(context,CampaignsDataActivity.class);
                campaignsDataIntent.putExtra("contactsName",contactsName);
                campaignsDataIntent.putExtra("campaignrow",singleRow);
                campaignsDataIntent.putExtra("contacts",contacts);
                campaignsDataIntent.putExtra("campaignArray", campaigns);
                context.startActivity(campaignsDataIntent);
            }
        });
        return rowView;
    }

}
