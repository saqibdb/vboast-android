package com.vboost.pro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;


import com.bigkoo.alertview.AlertView;
import com.bigkoo.alertview.OnDismissListener;
import com.bigkoo.alertview.OnItemClickListener;
import com.squareup.picasso.Picasso;
import com.vboost.pro.Adapter.CampaignsAdapter;
import com.vboost.pro.DBClasses.Company;
import com.vboost.pro.model.Campaigns;
import com.vboost.pro.model.ManageContacts;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nabeel Hafeez on 7/27/2016.
 */
public class CampaignsActivity extends AppCompatActivity implements OnItemClickListener, OnDismissListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_campaigns);

       List<Company> company =  Company.getAll();

        ImageView companyLogo = (ImageView) findViewById(R.id.campaign_company_logo);

        Picasso.with(this)
                .load(company.get(0).getLogo())
                .error(R.drawable.logo2)
                .into(companyLogo);


        ArrayList<Campaigns> campaignsArray = (ArrayList<Campaigns>) getIntent().getSerializableExtra("campaignArray");
        String[] contactsName = getIntent().getStringArrayExtra("contactsName");
        ArrayList<ManageContacts> contactsList = (ArrayList<ManageContacts>) getIntent().getSerializableExtra("contacts");

        CampaignsAdapter campaignsAdapter =  new CampaignsAdapter(this, campaignsArray, contactsList, contactsName, true);

        ListView listViewCampaigns = (ListView) findViewById(R.id.listView_campaignc);
        listViewCampaigns.setAdapter(campaignsAdapter);


    }

    public void alertMenuCampaign(View view) {
        new AlertView(null, null, null, null,
                new String[]{"Manage Contacts",
                        "Switch Companies",
                        "Get Help",
                },
                this, AlertView.Style.Alert, this).setCancelable(true).setOnDismissListener(this).show();
    }

    @Override
    public void onItemClick(Object o, int position) {

        if (position == 0) {

            Intent contactIntent = new Intent(CampaignsActivity.this, ManageContatcsAvtivity.class);
            startActivity(contactIntent);
            finish();

        } else if (position == 1) {

            Intent logoutIntent = new Intent(CampaignsActivity.this, LogOutActivity.class);
            startActivity(logoutIntent);

        } else if (position == 2) {

            Intent helpIntent = new Intent(CampaignsActivity.this, GetHelpActivity.class);
            startActivity(helpIntent);
        }


    }

    @Override
    public void onDismiss(Object o) {


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
//            if(mAlertView!=null && mAlertView.isShowing()){
//                mAlertView.dismiss();
//                return false;
//            }
        }

        return super.onKeyDown(keyCode, event);

    }



}
